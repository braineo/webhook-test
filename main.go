package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"

	"github.com/xanzy/go-gitlab"
)

func handleWebhook(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("headers: %v\n", r.Header)
	payload, err := ioutil.ReadAll(r.Body)
	event, err := gitlab.ParseHook(gitlab.HookEventType(r), payload)
	if err != nil {
		log.Printf("error reading request body: err=%s\n", err)
		return
	}
	defer r.Body.Close()
	// Check event
	// fmt.Printf("event received %s\n", event)
	switch event := event.(type) {
	case *gitlab.PushEvent:
		// processPushEvent(event)
		// ignore push event
	case *gitlab.MergeEvent:
		processMergeEvent(event)
	default:
		log.Printf("unknown event type %v\n", gitlab.WebhookEventType(r))
	}
}

func processMergeEvent(event *gitlab.MergeEvent) {
	fmt.Printf("description %s\n", event.ObjectAttributes.Description)
	pattern, err := regexp.Compile(`\b((?:[Cc]los(?:e[sd]?|ing)|\b[Ff]ix(?:e[sd]|ing)?|\b[Rr]esolv(?:e[sd]?|ing)|\b[Ii]mplement(?:s|ed|ing)?)(:?) +(?:(?:issues? +)?#(\d+)(?:(?: *,? +and +| *,? *)?)|([A-Z][A-Z0-9_]+-\d+))+)`)
	if err != nil {
		log.Println("wrong issue regex pattern")
	}
	fmt.Printf("%#v\n", pattern.FindAllStringSubmatch(event.ObjectAttributes.Description, -1))
	matches := pattern.FindAllStringSubmatch(event.ObjectAttributes.Description, -1)
	issueKeys := make([]string, 0)
	for _, match := range matches {
		if match[len(match)-1] != "" {
			issueKeys = append(issueKeys, match[len(match)-1])
		}
	}
	fmt.Printf("Adding issues to Review %v\n", issueKeys)
	for _, key := range issueKeys {
		fmt.Printf("Processing issue %v\n", key)
	}
}

func main() {
	log.Println("server started")
	fmt.Printf("Hello, World!\n")
	http.HandleFunc("/webhook", handleWebhook)
	log.Fatal(http.ListenAndServe("localhost:8080", nil))
}
